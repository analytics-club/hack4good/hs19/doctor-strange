# ######################################## #
# Git Repository for the SMEB Prediction / Forecast team. #
# ######################################## #

# To run our scripts, please follow the following instructions:

# Step 1: Downloading the data
We have provided all necessary raw data files on the shared Google Drive folder in "[H4G]2nd edition - IMPACT/Data and Resources/Data/data_forecast". Put this data in /data/raw.

If you do not have access to this folder / More details: We use two raw data files.
File 1: "reach_syr_dataset_market monitoring_redesign_august2019_without_first_row.xlsx": A slightly modified version of the original data file "reach_syr_dataset_market monitoring_redesign_august2019.xlsx": Remove the first row of the sheet "Subdistrict_Time Series" manually.
File 2: "impute_supercoarse.csv":
This is a file given to us by the imputation team. It contains data from all subdistricts and all months and all separate item prices (flour, water etc.). SMEB can be calculated from this file by simply summing up all column prices and multiplying the result by the float value.

# Step 2: Run the data cleaning script
Proceed by executing (in Python) the script /src/data_cleaning/clean_data.py. This will generate the processed, cleaned data. For further information on what this file does, please read the comments in this file.
Please ensure that the following modules are installed: pandas, numpy, os

# Step 3: Run the models
All of our models are located in /src/models. Follow the instructions in the README file in this folder.

